import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from '../models/lista-pokemons.model';

@Component({
  selector: 'pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css'],
})
export class PokemonCardComponent implements OnInit {
  @Input() pokemon: Pokemon;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-6';
  @Output() clicked: EventEmitter<Pokemon>
  pokemons: Pokemon[];

  constructor() {
    this.clicked = new EventEmitter();  
  }

  ngOnInit(): void {}

  ir(){
    this.clicked.emit(this.pokemon)
    return false;
  }

}
