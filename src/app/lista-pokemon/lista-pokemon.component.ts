import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../models/lista-pokemons.model';
@Component({
  selector: 'lista-pokemon',
  templateUrl: './lista-pokemon.component.html',
  styleUrls: ['./lista-pokemon.component.css'],
})
export class ListaPokemonComponent implements OnInit {
  pokemons: Pokemon[];
 
  constructor() {
    this.pokemons = [];
  }

  guardar(nombre: string, tipo: string, descripcion: string): boolean {
    this.pokemons.push(new Pokemon(nombre, tipo, descripcion));
    console.log(this.pokemons);
    return false;
  }
  elegido(pokemon: Pokemon){
    this.pokemons.forEach(function (x) {x.setSelected(false)})
    pokemon.setSelected(true);
  }
  ngOnInit(): void {}
}
