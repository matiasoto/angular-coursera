export class Pokemon {
  private selected: boolean;
  public powers: string[];
  constructor(public nombre: string, public tipo: string, public descripcion: string) {
    this.powers = ['fuego', 'electricidad', 'agua']
  }
  isSelected(): boolean{
    return this.selected;
  }
  setSelected(s:boolean){
    this.selected=s;
  }
}
